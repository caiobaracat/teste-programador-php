<?php
    if ($this->request->query->get('status') == 'asc') {
        $status = 'desc';
    } else {
        $status = 'asc';
    }

    if ($this->request->query->get('situacao') == 'asc') {
        $situacao = 'desc';
    } else {
        $situacao = 'asc';
    }
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Início</th>
                    <th>Fim</th>
                    <th>
                        <a href="/?status=<?= $status ?>">
                            Status
                        </a>
                    </th>
                    <th>
                        <a href="/?situacao=<?= $situacao ?>">
                            Situação
                        </a>
                    </th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                <?php if ($atividades): ?>
                    <?php foreach ($atividades as $atividade): ?>
                        <tr>
                            <td><?= $atividade->nome ?></td>
                            <td><?= $atividade->descricao ?></td>
                            <td><?= date('d/m/Y', strtotime($atividade->inicio)) ?></td>
                            <td><?= date('d/m/Y', strtotime($atividade->fim)) ?></td>
                            <td><?= $atividade->status ?></td>
                            <td>
                                <?php if ($atividade->situacao_id == 4): ?>
                                    <span class="label label-success"><?= $atividade->situacao ?></span>
                                <?php else : ?>
                                    <span class="label label-primary"><?= $atividade->situacao ?></span>
                                <?php endif ?>
                            </td>
                            <td>
                                <?php if ($atividade->situacao_id == 4): ?>
                                    <a href="#" class="btn btn-default btn-sm" <?= ($atividade->situacao_id == 4) ? 'disabled' : '' ?>>Alterar</a>
                                <?php else : ?>
                                    <a href="/alterar/<?= $atividade->id ?>" class="btn btn-default btn-sm" <?= ($atividade->situacao_id == 4) ? 'disabled' : '' ?>>Alterar</a>
                                <?php endif ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
            </tbody>
        </table>
        <a href="/nova" class="btn btn-success btn-sm">Nova Atividade</a>
    </div>
</div>
