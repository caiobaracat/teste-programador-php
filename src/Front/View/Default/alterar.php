<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <form action="" class="horizontal-form" method="post">
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="<?= $atividade->nome; ?>">
            </div>
            <div class="form-group">
                <label for="descricao">Descrição</label>
                <textarea name="descricao" id="descricao" class="form-control">
                    <?= $atividade->descricao; ?>
                </textarea>
            </div>
            <div class="form-group">
                <label for="inicio">Início</label>
                <input name="inicio" id="inicio" class="form-control" value="<?= $atividade->inicio ?>" type="text" />
            </div>
            <div class="form-group">
                <label for="fim">Fim</label>
                <input name="fim" id="fim" class="form-control" value="<?= $atividade->fim ?>" type="text" />
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="ativo" <?= ($atividade->status == 'ativo') ? 'selected' : ''; ?>>Ativo</option>
                    <option value="inativo" <?= ($atividade->status == 'inativo') ? 'selected' : ''; ?>>Inativo</option>
                </select>
            </div>
            <div class="form-group">
                <label for="situacao">Sitaução</label>
                <select name="situacao" id="situacao" class="form-control">
                    <?php foreach ($situacoes as $situacao): ?>
                        <?php if ($situacao->id == $atividade->situacao_id): ?>
                            <option selected value="<?= $situacao->id; ?>"><?= $situacao->titulo; ?></option>
                        <?php else : ?>
                            <option value="<?= $situacao->id; ?>"><?= $situacao->titulo; ?></option>
                        <?php endif ?>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-success btn-sm">Alterar</button>
                <a href="/" class="btn btn-default btn-sm">Voltar</a>
            </div>
        </form>
    </div>
</div>
