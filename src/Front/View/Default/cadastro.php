<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <form id="form-new" class="horizontal-form" method="post">
            <div class="form-group">
                <label for="nome">Nome</label>
                <input
                    type="text"
                    class="form-control"
                    id="nome"
                    name="nome"
                    placeholder="Nome"
                    data-rule-required="Este campo é obrigatório.">
            </div>
            <div class="form-group">
                <label for="descricao">Descrição</label>
                <textarea name="descricao" id="descricao" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label for="inicio">Início</label>
                <input name="inicio" id="inicio" class="form-control" type="text" />
            </div>
            <div class="form-group">
                <label for="fim">Fim</label>
                <input name="fim" id="fim" class="form-control" type="text" />
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="ativo">Ativo</option>
                    <option value="inativo">Inativo</option>
                </select>
            </div>
            <div class="form-group">
                <label for="situacao">Sitaução</label>
                <select name="situacao" id="situacao" class="form-control">
                    <?php foreach ($situacoes as $situacao): ?>
                    <option value="<?= $situacao->id; ?>"><?= $situacao->titulo; ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-success btn-sm">Cadastrar</button>
                <a href="/" class="btn btn-default btn-sm">Início</a>
            </div>
        </form>
    </div>
</div>
