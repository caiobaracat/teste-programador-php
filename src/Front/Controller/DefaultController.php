<?php
namespace Front\Controller;

use App\Framework\Controller\Controller;
use Front\Entity\Situacao;

class DefaultController extends Controller
{
    public function index()
    {
        $atividadesRepository = $this->setRepository('Front', 'Atividades');
        if ($this->request->query->get('status')) {
            $order = ['status' => $this->request->query->get('status')];
        } elseif ($this->request->query->get('situacao')) {
            $order = ['situacao' => $this->request->query->get('situacao')];
        } else {
            $order = ['a.id' => 'desc'];
        }

        $options = [
            'order' => $order
        ];
        return [
            'template' => 'index',
            'parameters' => [
                'atividades' => $atividadesRepository->findAll($options)
            ]
        ];
    }

    public function cadastro()
    {
        $situacaoRepository = $this->setRepository('Front', 'Situacao');
        if ($this->request->isMethod('post')) {
            $atividadesRepository = $this->setRepository('Front', 'Atividades');
            if ($atividadesRepository->cadastrar($this->request->request)) {
                header('Location: /');
            }
        }

        return [
            'template' => 'cadastro',
            'parameters' => [
                'situacoes' => $situacaoRepository->findAll()
            ]
        ];
    }

    //public function alterar()
    public function alterar($id)
    {
        if ($id == null) {
            return ['template' => '403'];
        }

        $situacaoRepository = $this->setRepository('Front', 'Situacao');
        $atividadesRepository = $this->setRepository('Front', 'Atividades');
        $atividade = $atividadesRepository->findById($id);

        if ($atividade->situacao_id == 4) {
            return ['template' => '403'];
        }
        if ($this->request->isMethod('post')) {
            if (!$atividadesRepository->alterar($id, $this->request->request)) {
                return ['template' => '500'];
            }
        }

        return [
            'template' => 'alterar',
            'parameters' => [
                'atividade' => $atividade,
                'situacoes' => $situacaoRepository->findAll()
            ]
        ];
    }
}
