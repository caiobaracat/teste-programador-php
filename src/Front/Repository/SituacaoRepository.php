<?php
namespace Front\Repository;

use App\Framework\Model\Repository;

class SituacaoRepository extends Repository
{
    public function findAll()
    {
        $stmt = $this->pdo->prepare('SELECT * FROM situacao');
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return $stmt->fetchAll(\PDO::FETCH_OBJ);
        }

        return false;
    }
}
