<?php
namespace Front\Repository;

use App\Framework\Model\Repository;
use Symfony\Component\HttpFoundation\ParameterBag;

class AtividadesRepository extends Repository
{
    public function findAll(array $options = null)
    {
        if (!is_null($options)) {
            if ($options['order']) {
                $key = key($options['order']);
                $value = $options['order'][$key];

                $order = "ORDER BY {$key} {$value}";
            }
        }
        $stmt = $this->pdo->prepare("select
            a.*,
            s.titulo as situacao
        from
            atividade a
        inner join situacao s on s.id = a.situacao_id " . $order);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return $stmt->fetchAll(\PDO::FETCH_OBJ);
        }

        return false;
    }

    public function findById($id)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM atividade WHERE id = :id');
        $stmt->bindValue(':id', $id);

        if ($stmt->execute()) {
            return $stmt->fetch(\PDO::FETCH_OBJ);
        }

        return false;
    }

    public function cadastrar(ParameterBag $request)
    {
        $stmt = $this->pdo->prepare("INSERT INTO `teste`.`atividade` (`nome`, `descricao`, `inicio`, `fim`, `status`, `situacao_id`) VALUES (:nome, :descricao, :inicio, :fim, :status, :situacao)");

        $stmt->bindValue(':nome', $request->get('nome'));
        $stmt->bindValue(':descricao', trim($request->get('descricao')));
        $stmt->bindValue(':status', $request->get('status'));
        $stmt->bindValue(':situacao', $request->get('situacao'));
        $stmt->bindValue(':inicio', date('Y-m-d', strtotime($request->get('inicio'))));
        $stmt->bindValue(':fim', date('Y-m-d', strtotime($request->get('fim', null))));

        return $stmt->execute();
    }

    public function alterar($id, ParameterBag $request)
    {
        $stmt = $this->pdo->prepare("UPDATE `atividade` SET `nome` = :nome, `descricao` = :descricao, `inicio` = :inicio,  `fim` = :fim, `status` = :status, `situacao_id` = :situacao WHERE id = :id");

        $stmt->bindValue(':nome', $request->get('nome'));
        $stmt->bindValue(':descricao', trim($request->get('descricao')));
        $stmt->bindValue(':status', $request->get('status'));
        $stmt->bindValue(':situacao', $request->get('situacao'));
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':fim', $request->get('fim'));
        $stmt->bindValue(':inicio', $request->get('inicio'));

        return $stmt->execute();
    }
}
