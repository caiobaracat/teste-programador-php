# Demonstrativo para vaga de Programador PHP #


### Dependencias ###

* Symfony/Routing
* Symfony/Request
* php5.6 +
* mysql 5.6 +
* composer

### Instruções ###

* Após clonar o repositório, rodar 'composer install' na raíz do projeto
* Informar as diretivas de banco de dadso em 'App/Config/parameters.yml'
* Criar o banco de dados contido no arquivo 'schema.sql' na raíz do projeto

Observação: Não vi onde encaixar uma Procedure no banco de dados,
como sugeriam as especificações. Portanto, este quesito não foi atendido.

Em caso de dúvidas, entrar em contato.