-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema teste
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `teste` ;

-- -----------------------------------------------------
-- Schema teste
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `teste` DEFAULT CHARACTER SET latin1 ;
USE `teste` ;

-- -----------------------------------------------------
-- Table `teste`.`situacao`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `teste`.`situacao` ;

CREATE TABLE IF NOT EXISTS `teste`.`situacao` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `teste`.`atividade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `teste`.`atividade` ;

CREATE TABLE IF NOT EXISTS `teste`.`atividade` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NULL DEFAULT NULL,
  `descricao` VARCHAR(600) NULL DEFAULT NULL,
  `inicio` DATE NULL DEFAULT NULL,
  `fim` DATE NULL DEFAULT NULL,
  `status` ENUM('ativo', 'inativo') NULL DEFAULT NULL,
  `situacao_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_atividade_situacao_idx` (`situacao_id` ASC),
  CONSTRAINT `fk_atividade_situacao`
    FOREIGN KEY (`situacao_id`)
    REFERENCES `teste`.`situacao` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
