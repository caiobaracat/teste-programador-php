$('form').validate({
	rules: {
		nome: {
			required: true,
			maxlength: 255,
		},
		descricao: {
			required: true,
			maxlength: 600,
		},
		inicio: {
			required: true,
			date: true
		},
		fim: {
			required: {
				depends: function(e) {
					return $('#situacao').val() == 4;
				}
			}
		},
		status: {
			required: true
		},
		situacao: {
			required: true
		}
	},
	messages: {
		nome: {
			required: 'Este campo é obrigatório',
			maxlength: 'Você pode digitar apenas 255 caracteres',
		},
		descricao: {
			required: 'Este campo é obrigatório',
			maxlength: 'Você pode digitar apenas 600 caracteres',
		},
		inicio: {
			required: 'Este campo é obrigatório',
			date: 'Informe uma data no formato AAAA-MM-DD',
		},
		fim: {
			required: 'Este campo é obrigatório',
			date: 'Informe uma data no formato AAAA-MM-DD',
		},
		status: {
			required: 'Este campo é obrigatório'
		},
		situacao: {
			required: 'Este campo é obrigatório'
		}
	}
});
