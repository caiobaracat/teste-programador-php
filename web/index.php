<?php
use App\Config\Kernel;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../vendor/autoload.php';

try {
    $kernel = new Kernel;
    $request = Request::createFromGlobals();
    $response = $kernel->handle($request);
    $response->send();
} catch (Exception $e) {
    $e->getMessage();
} catch (Exception\HttpNotFoundException $e) {
    $e->getMessage();
}
