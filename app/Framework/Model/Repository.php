<?php
namespace App\Framework\Model;

use App\Framework\Model\Database;

class Repository
{
    protected $pdo;

    public function __construct()
    {
        $this->pdo = (new Database)->getDatabase();
    }
}
