<?php
namespace App\Framework\Model;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Yaml\Yaml;

class Database
{
    private $pdo;

    private $parameters;

    public function __construct()
    {
        $this->setup();
    }

    public function getDatabase()
    {
        return $this->pdo;
    }

    private function setup()
    {
        $this->loadParameters();

        try {
            $dsn = "mysql:host={$this->parameters['host']};dbname={$this->parameters['name']}";
            $this->pdo = new \PDO($dsn, $this->parameters['user'], $this->parameters['password']);
        } catch (\PDOException $e) {
            echo $e->getMessage();
            exit();
        }
    }

    private function loadParameters()
    {
        $path = __DIR__ . '/../../Config';

        $locator = new FileLocator(array($path));
        $this->parameters = Yaml::parse(file_get_contents($locator->locate('parameters.yml')))['database'];
    }
}
