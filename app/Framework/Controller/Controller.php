<?php
namespace App\Framework\Controller;

use Symfony\Component\HttpFoundation\Request;
use App\Framework\Model\Database;

class Controller
{
    protected $request;

    private $vendor;

    private $controller;

    protected $database;

    public function __construct(Request $request, $vendor, $controller)
    {
        $this->request    = $request;
        $this->vendor     = $vendor;
        $this->controller = $controller;

        $this->$controller = $this->setRepository($this->vendor, $this->controller);
    }

    public function setRepository($vendor, $controller, $property = null)
    {
        $namespace = $vendor . '\\Repository\\' . $controller . 'Repository';

        if (class_exists($namespace)) {
            return new $namespace;
        }

        return null;
    }
}
