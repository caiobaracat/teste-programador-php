<?php
namespace App\Config;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;

class Kernel
{
    private $request;

    private $routeCollection;

    private $route;

    private $vendor;

    private $controller;

    private $action;

	public function __construct()
	{
        $this->configure();
	}

	public function handle(Request $request)
	{
		$this->request = $request;

        if (!$this->routing()) {
            throw new HttpNotFoundException("O caminho especiaifcado não foi encontrado.");
        }

        $this->vendor     = explode(':', $this->route['_controller'])[0];
        $this->controller = explode(':', $this->route['_controller'])[1];
        $this->action     = explode(':', $this->route['_controller'])[2];

        $namespace = $this->vendor . '\\' . 'Controller\\' . $this->controller . 'Controller';

        if (class_exists($namespace)) {
            $class = new $namespace($request, $this->vendor, $this->controller);

            $parameters = $this->route;
            unset($parameters['_controller']);
            unset($parameters['_route']);

            $method = call_user_func_array(array($class, $this->action), $parameters);

            if (is_array($method) && !empty($method)) {
                return new Response($this->render($method, $this->action));
            }

            throw new \RuntimeException("O método {$this->action} deve retornar um array");
        }

        throw new RuntimeException("A classe {$namespace} não pôde ser localizada");
	}

    private function configure()
    {
        $locator = new FileLocator(array(__DIR__ . '/routing'));
        $loader = new YamlFileLoader($locator);

        $this->routeCollection = $loader->load('routes.yml');
    }

	private function routing()
	{
        $context = new RequestContext();
        $context->fromRequest($this->request);
        $matcher = new UrlMatcher($this->routeCollection, $context);
        $this->route = $matcher->match($this->request->server->get('REQUEST_URI'));

        if (!$this->route) {
            return false;
        }

        return true;
	}

    private function render(array $render, $action)
    {
        if (!array_key_exists('template', $render)) {
            throw new RuntimeException("O array retornado pelo médoto {$action} deve conter uma chave 'template' com o arquivo a ser renderizado pelo método.");
        }

        if (array_key_exists('parameters', $render)) {
            extract($render['parameters']);
        }

        if (array_key_exists('layout', $render)) {
            $layout = $render['layout'];
        } else {
            $layout = 'layout.php';
        }

        $templatePath = '/../../src/' . $this->vendor . '/View/' . $this->controller . '/' . $render['template'] . '.php';

        ob_start();
        require_once(__DIR__ . $templatePath);
        $content = ob_get_clean();

        ob_start();
        require(__DIR__ . '/../Views/' . $layout);
        $layoutContent = ob_get_clean();

        return $layoutContent;
    }
}
