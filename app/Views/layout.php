<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Teste</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/main.css" />
</head>
<body>
    <header>
        <nav>
            <h5>Gerenciador de Atividades</h5>
        </nav>
    </header>

    <div class="container">
        <?= $content; ?>
    </div>

    <footer>
        <small>Desenvolvido por <a href="mailto:caio.baracat@fatec.sp.gov.br">Caio Baracat</a></small>
    </footer>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/aditional-methods.min.js"></script>

    <script src="/js/main.js"></script>
</body>
</html>
